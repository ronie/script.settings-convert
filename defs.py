TYPES = ['sep', 'lsep', 'text', 'ipaddress', 'number', 'slider', 'date', 
         'time', 'bool', 'select', 'addon', 'enum', 'labelenum', 'file', 
         'audio', 'video', 'image', 'executable', 'folder', 'fileenum', 
         'action', 'rangeofnum', '']

ATTRIBS = ['label', 'type', 'source', 'visible', 'enable', 'subsetting', 
           'id', 'default', 'option', 'range', 'values', 'lvalues', 
           'addontype', 'mask', 'multiselect', 'entries', 'sort', 
           'rangestart', 'rangeend', 'elements', 'valueformat', 'action']

